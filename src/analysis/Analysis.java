package analysis;

import java.util.Scanner;

/**
 * Calculates sides need to get every 5th hypotenuses to {@code n} and prints result's outputs.
 *
 * @author Vladislav
 * @version 1.0
 * @since 02.14.2020
 */
public class Analysis {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n, x, y, z;

        System.out.print("Please, enter a number: ");
        n = input.nextInt();

        x = 1;

        while(x < n) {
            y = x;

            while(y <= n) {
                z = y;

                while(z <= n) {
                    if(z <= x * 2 && y <= x * 2 && z * z == x * x + y * y) {
                        System.out.println("\n" + x + " " + y + " " + z + " ");
                    }

                    z++;
                }

                y++;
            }

            x++;
        }
    }
}