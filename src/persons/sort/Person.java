package persons.sort;

public class Person {
    private String surname;
    private int year;

    Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    int getYear() {
        return year;
    }

    String getSurname() {
        return surname;
    }

    public String toString() {
        return surname + "{Year of birth: " + year + '}';
    }
}