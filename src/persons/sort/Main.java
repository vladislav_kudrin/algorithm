package persons.sort;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Sorts elements in {@code persons} by year of birth and surnames by insertion sort method.
 * Searches element in {@code persons} by {@code value} by binary search method.
 * Searches elements in {@code persons} by {@code value} by binary search method and adds results to {@code homonyms}.
 *
 * @author Vladislav
 * @version 1.0
 * @since 02.10.2020
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String value;
        Person[] persons = new Person[5];
        ArrayList<Person> homonyms = new ArrayList<>();

        persons[0] = new Person("Vasya", 2202);
        persons[1] = new Person("Sosiska", 0);
        persons[2] = new Person("Batya", 7894);
        persons[3] = new Person("Babka", 1010);
        persons[4] = new Person("Nxfh", 2001);

        System.out.println("Original persons:");
        output(persons);

        yearsSort(persons);

        System.out.println("\nPersons sorted by years:");
        output(persons);

        surnamesSort(persons);

        System.out.println("\nPersons sorted by surnames:");
        output(persons);

        System.out.print("\nPlease, enter a surname that you want to find: ");
        value = input.nextLine();

        System.out.println("\nResult:\n" + surnamesBinarySearch(value, persons));

        System.out.print("\nPlease, enter a surname whose homonyms you want to find: ");
        value = input.nextLine();

        findHomonyms(value, persons, homonyms);

        System.out.println("\nHomonyms:");
        output(homonyms);
    }

    /**
     * Sorts elements in {@code persons} by year of birth by insertion sort method.
     *
     * @param persons an one-dimensional array contains elements of Person type.
     */
    private static void yearsSort(Person[] persons) {
        int index;
        Person temporary;

        for(int queue = 1; queue < persons.length; queue++) {
            index = queue;
            temporary = persons[queue];

            while(index > 0 && persons[index - 1].getYear() <= temporary.getYear()) {
                persons[index] = persons[index - 1];
                index--;
            }

            persons[index] = temporary;
        }
    }

    /**
     * Sorts elements in {@code persons} by surnames by insertion sort method.
     *
     * @param persons the one-dimensional array contains elements of Person type.
     */
    private static void surnamesSort(Person[] persons) {
        Person temporary;
        int index;

        for(int queue = 1; queue < persons.length; queue++) {
            temporary = persons[queue];
            index = queue;

            while(index > 0 && persons[index - 1].getSurname().compareTo(temporary.getSurname()) >= 0) {
                persons[index] = persons[index - 1];
                index--;
            }

            persons[index] = temporary;
        }
    }

    /**
     * Searches element in {@code persons} with {@code value} and returns either found element or "No result!".
     *
     * @param value a surname using to search an element in {@code persons} with the same surname.
     * @param persons the one-dimensional array contains elements of Person type.
     * @return either found element or "No result!".
     */
    private static String surnamesBinarySearch(String value, Person[] persons) {
        int firstIndex = 0, middleIndex, lastIndex = persons.length - 1;
        String middleValue;

        while(firstIndex <= lastIndex) {
            middleIndex = (firstIndex + lastIndex) / 2;
            middleValue = persons[middleIndex].getSurname();

            if(middleValue.compareTo(value) == 0) {
                return persons[middleIndex].toString();
            }
            else if(middleValue.compareTo(value) < 0) {
                firstIndex = middleIndex + 1;
            }
            else {
                lastIndex = middleIndex - 1;
            }
        }

        return "No result!";
    }

    /**
     * Searches elements in {@code persons} with {@code value} and adds them to {@code homonyms}.
     *
     * @param value a surname using to search elements in {@code persons} with the same surname.
     * @param persons the one-dimensional array contains elements of Person type.
     * @param homonyms found elements in {@code persons} by {@code value}.
     */
    private static void findHomonyms(String value, Person[] persons, ArrayList<Person> homonyms) {
        boolean flag;
        int firstIndex = 0, middleIndex = 0, lastIndex = persons.length - 1;
        String middleValue;

        do {
            flag = false;
            while(firstIndex <= lastIndex && middleIndex != (firstIndex + lastIndex) / 2) {
                middleIndex = (firstIndex + lastIndex) / 2;
                middleValue = persons[middleIndex].getSurname();

                if(middleValue.compareTo(value) == 0) {
                    flag = true;
                    homonyms.add(persons[middleIndex]);
                }
                else if(middleValue.compareTo(value) < 0) {
                    firstIndex = middleIndex + 1;
                }
                else {
                    lastIndex = middleIndex - 1;
                }
            }
        } while(flag);
    }

    /**
     * Outputs values of elements in {@code persons}.
     *
     * @param persons the one-dimensional array contains elements of Person type.
     */
    private static void output(Person[] persons) {
        for(Person person : persons) {
            System.out.println(person);
        }
    }

    /**
     * Outputs values of elements in {@code homonyms}.
     *
     * @param homonyms the list contains elements of Person type.
     */
    private static void output(ArrayList<Person> homonyms) {
        for(Person person : homonyms) {
            System.out.println(person);
        }
    }
}