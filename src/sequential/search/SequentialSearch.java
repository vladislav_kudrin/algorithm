package sequential.search;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A class sequential searching an index of values in one dimensional array and ArrayList.
 *
 *
 *
 * @author Vladislav
 */
public class SequentialSearch {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int index, listIndex;
        int value, listValue;
        int[] array = {5, 2, -12, 67, 4, 342, -3};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(3, -6, -4, 18, 46, -5, 7));

        System.out.print("Please, enter a number for sequential searching in one dimensional array: ");
        value = input.nextInt();
        System.out.print("Please, enter a number for sequential searching in ArrayList: ");
        listValue = input.nextInt();

        index = arraysSequentialSearch(value, array);
        listIndex = listsSequentialSearch(listValue, list);

        System.out.println("\n" + ((index == -1) ? "No result" : "Value " + value + " contains in one dimensional array at index: " + index));
        System.out.println(((listIndex == -1) ? "No result" : "Value " + listValue + " contains in ArrayList at index: " + listIndex));
    }

    /**
     * A method sequential searching an index of value in one dimensional array.
     *
     * @param value a value for searching.
     * @param array the one dimensional array.
     * @return either an index of one dimensional array contains a value of -1.
     */
    private static int arraysSequentialSearch(int value, int[] array) {
        for(int index = 0; index < array.length; index++) {
            if(value == array[index]) {
                return index;
            }
        }

        return -1;
    }

    /**
     * A method sequential searching an index of value in ArrayList.
     *
     * @param value a value for searching.
     * @param list the ArrayList.
     * @return either an index of ArrayList contains a value or -1.
     */
    private static int listsSequentialSearch(int value, ArrayList list) {
        return (list.contains(value)) ? list.indexOf(value) : -1;
    }
}