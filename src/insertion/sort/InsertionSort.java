package insertion.sort;

import java.util.Arrays;
import java.util.ArrayList;

/**
 * A class sorts array's and list's values by insertion sort method.
 *
 *
 *
 * @author Vladislav
 */
public class InsertionSort {
    public static void main(String[] args) {
        int[] array = {-4, 3, 0, 45, 23, -25};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(7, 4, 13, -5, 0, 65));

        System.out.println("Start array: " + Arrays.toString(array));
        System.out.println("Start list: " + list);

        insertionSort(array);
        insertionSort(list);

        System.out.println("\nSorted array: " + Arrays.toString(array));
        System.out.println("Sorted list: " + list);
    }

    /**
     * A method sorts {@code array} in ascending by insertion sort method.
     *
     * @param array an one dimensional array with values need to sort.
     */
    private static void insertionSort(int[] array) {
        int temporary, index;

        for(int queue = 1; queue < array.length; queue++) {
            temporary = array[queue];
            index = queue;

            while(index > 0 && array[index - 1] >= temporary) {
                array[index] = array[index - 1];
                index--;
            }

            array[index] = temporary;
        }
    }

    /**
     * A method sorts {@code list} in descending by insertion sort method.
     *
     * @param list a list with values that need to sort.
     */
    private static void insertionSort(ArrayList<Integer> list) {
        int temporary, index;

        for(int queue = 1; queue < list.size(); queue++) {
            temporary = list.get(queue);
            index = queue;

            while(index > 0 && list.get(index - 1) <= temporary) {
                list.set(index, list.get(index - 1));
                index--;
            }

            list.set(index, temporary);
        }
    }
}