package quick.sort;

import java.util.Arrays;
import java.util.ArrayList;

/**
 * Sorts {@code array} and {@code list} values by quick sort method.
 *
 * @author Vladislav
 * @since 06/09/2020
 * @version 1.0
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] array = {85, 12, -4, 54, 1, 0};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(15, -13, 77, -42, 0, 3));

        System.out.println("Start array: " + Arrays.toString(array));
        System.out.println("Start list: " + list);

        quickSort(array, 0, array.length - 1);
        quickSort(list, 0, list.size() - 1);

        System.out.println("\nSorted array: " + Arrays.toString(array));
        System.out.println("Sorted list: " + list);
    }

    /**
     * Sorts {@code array} by quick sort method.
     *
     * @param array an one-dimensional array with values that need to sort.
     * @param lowIndex a lower index of {@code array}.
     * @param highIndex a higher index of {@code array}.
     */
    private static void quickSort(int[] array, int lowIndex, int highIndex) {
        if(array.length < 2) return;

        int changer;
        int leftIndex = lowIndex;
        int rightIndex = highIndex;
        int pivotValue = array[(lowIndex + highIndex) / 2];

        while(leftIndex <= rightIndex) {
            while(array[leftIndex] < pivotValue) leftIndex++;

            while(array[rightIndex] > pivotValue) rightIndex--;

            if(leftIndex <= rightIndex) {
                changer = array[leftIndex];
                array[leftIndex] = array[rightIndex];
                array[rightIndex] = changer;
                leftIndex++;
                rightIndex--;
            }
        }

        if(lowIndex < rightIndex) quickSort(array, lowIndex, rightIndex);

        if(highIndex > leftIndex) quickSort(array, leftIndex, highIndex);
    }

    /**
     * Sorts {@code array} by quick sort method.
     *
     * @param list an ArrayList with values that need to sort.
     * @param lowIndex a lower index of {@code list}.
     * @param highIndex a higher index of {@code list}.
     */
    private static void quickSort(ArrayList<Integer> list, int lowIndex, int highIndex) {
        if(list.size() < 2) return;

        int changer;
        int leftIndex = lowIndex;
        int rightIndex = highIndex;
        int pivotValue = list.get((lowIndex + highIndex) / 2);

        while(leftIndex <= rightIndex) {
            while(list.get(leftIndex) < pivotValue) leftIndex++;

            while(list.get(rightIndex) > pivotValue) rightIndex--;

            if(leftIndex <= rightIndex) {
                changer = list.get(leftIndex);
                list.set(leftIndex, list.get(rightIndex));
                list.set(rightIndex, changer);
                leftIndex++;
                rightIndex--;
            }
        }

        if(lowIndex < rightIndex) quickSort(list, lowIndex, rightIndex);

        if(highIndex > leftIndex) quickSort(list, leftIndex, highIndex);
    }
}