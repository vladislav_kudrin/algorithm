package selection.sort;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A class sorts array's and list's values by selection sort method.
 *
 *
 *
 * @author Vladislav
 */
public class SelectionSort {
    public static void main(String[] args) {
        int[] array = {9, 54, 42, 32, 6, 99, 13, 5};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(15, 42, 36, 66, 12, 8, 10, 43));

        System.out.println("Start array: " + Arrays.toString(array));
        System.out.println("Start list: " + list);

        selectionSort(array);
        selectionSort(list);

        System.out.println("\nSorted array: " + Arrays.toString(array));
        System.out.println("Sorted list: " + list);
    }

    /**
     * A method sorts {@code array} by selection sort method.
     *
     * @param array an array with values that need to sort.
     */
    private static void selectionSort(int[] array) {
        int minimumValuesIndex, changer;

        for(int queue = 0; queue < array.length - 1; queue++) {
            minimumValuesIndex = queue;

            for(int index = queue + 1; index < array.length; index++) {
                minimumValuesIndex = (array[index] < array[minimumValuesIndex]) ? index : minimumValuesIndex;
            }

            changer = array[queue];
            array[queue] = array[minimumValuesIndex];
            array[minimumValuesIndex] = changer;
        }
    }

    /**
     * A method sorts {@code list} by selection sort method.
     *
     * @param list a list with values that need to sort.
     */
    private static void selectionSort(ArrayList<Integer> list) {
        int minimalValuesIndex, changer;

        for(int queue = 0; queue < list.size() - 1; queue++) {
            minimalValuesIndex = queue;

            for(int index = queue + 1; index < list.size(); index++) {
                minimalValuesIndex = (list.get(index) < list.get(minimalValuesIndex)) ? index : minimalValuesIndex;
            }

            changer = list.get(queue);
            list.set(queue, list.get(minimalValuesIndex));
            list.set(minimalValuesIndex, changer);
        }
    }
}