package bubble.sort;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A class sorts array's and list's values by bubble sort method.
 *
 *
 *
 * @author Vladislav
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {56, 7, 2, 768, 23, 3, 10};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(44, 32, 5, 78, 12, 41, 0));

        System.out.println("Start array: " + Arrays.toString(array));
        System.out.println("Start list: " + list);

        bubbleSort(array);
        bubbleSort(list);

        System.out.println("\nSorted array: " + Arrays.toString(array));
        System.out.println("Sorted list: " + list);
    }

    /**
     * A method sorts {@code array} by bubble sort method.
     *
     * @param array an array with values that need to sort.
     */
    private static void bubbleSort(int[] array) {
        int changer;
        boolean flag = false;

        for(int queue = 1; queue < array.length; queue++) {
            for(int index = 0; index < array.length - queue; index++) {
                flag = false;

                if(array[index] > array[index + 1]) {
                    flag = true;

                    changer = array[index];
                    array[index] = array[index + 1];
                    array[index + 1] = changer;
                }
            }

            if(!flag) {
                return;
            }
        }
    }

    /**
     * A method sorts {@code list} by bubble sort method.
     *
     * @param list a list with values that need to sort.
     */
    private static void bubbleSort(ArrayList<Integer> list) {
        int changer;
        boolean flag = false;

        for(int queue = 1; queue < list.size(); queue++) {
            for(int index = 0; index < list.size() - queue; index++) {
                flag = false;

                if(list.get(index) > list.get(index + 1)) {
                    flag = true;

                    changer = list.get(index);
                    list.set(index, list.get(index + 1));
                    list.set(index + 1, changer);
                }
            }

            if(!flag) {
                return;
            }
        }
    }
}