package fibonacci.numbers;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * A class calculates a Fibonacci number by index in integer, long, BigInteger types.
 *
 * @author Vladislav
 */
public class FibonacciNumbers {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number;

        do {
            System.out.print("Please, enter an index of Fibonacci number: ");
            number = input.nextInt();
        } while(number < 1);

        System.out.println("\nFibonacci number is " + fibonacci(number));
        System.out.println("Long Fibonacci number is " + longFibonacci(number));
        System.out.println("Big Integer Fibonacci number is " + bigIntegerFibonacci(number));

        System.out.println("\nFibonacci number calculated with recursion is " + recursionFibonacci(number));
    }

    /**
     * A method calculates a Fibonacci number by {@code number} in integer type.
     *
     * @param number a Fibonacci number index.
     * @return a Fibonacci number by {@code number} in integer type.
     */
    private static int fibonacci(int number) {
        if(number == 1) {
            return 1;
        }

        int fibonacciNumber = 0, previousNumber = 0, newNumber = 1;

        for(int index = 1; index < number; index++) {
            fibonacciNumber = previousNumber + newNumber;
            previousNumber = newNumber;
            newNumber = fibonacciNumber;
        }

        return fibonacciNumber;
    }

    /**
     * A method calculates a Fibonacci number by {@code number} by recursion.
     *
     * @param number a Fibonacci number index.
     * @return a Fibonacci number by {@code number} calculated by recursion.
     */
    private static int recursionFibonacci(int number) {
        return (number <= 1) ? number : recursionFibonacci(number - 1) + recursionFibonacci(number - 2);
    }

    /**
     * A method calculates a Fibonacci number by {@code number} in long type.
     *
     * @param number a Fibonacci number index.
     * @return a Fibonacci number by {@code number} in long type.
     */
    private static long longFibonacci(int number) {
        if(number == 1) {
            return 1;
        }

        long fibonacciNumber = 0, previousNumber = 0, newNumber = 1;

        for(int index = 1; index < number; index++) {
            fibonacciNumber = previousNumber + newNumber;
            previousNumber = newNumber;
            newNumber = fibonacciNumber;
        }

        return fibonacciNumber;
    }

    /**
     * A method calculates a Fibonacci number by {@code number} in BigInteger type.
     *
     * @param number a Fibonacci number index.
     * @return a Fibonacci number by {@code number} in BigInteger type.
     */
    private static BigInteger bigIntegerFibonacci(int number) {
        if(number == 1) {
            return BigInteger.ONE;
        }

        BigInteger fibonacciNumber = BigInteger.ZERO;
        BigInteger previousNumber = BigInteger.ZERO;
        BigInteger newNumber = BigInteger.ONE;

        for(int index = 1; index < number; index++) {
            fibonacciNumber = previousNumber.add(newNumber);
            previousNumber = newNumber;
            newNumber = fibonacciNumber;
        }

        return fibonacciNumber;
    }
}