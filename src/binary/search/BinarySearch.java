package binary.search;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * A class binary searches an index of values in {@code array} and {@code list}.
 *
 *
 *
 * @author Vladislav
 */
public class BinarySearch {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int index, listIndex;
        int value, listValue;
        int[] array = {-300, -122, -100, -75, 0, 58, 64, 100};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(-5000, -1000, -500, -1, 0, 1, 500, 1000, 5000));

        System.out.print("Please, enter a number for a binary searching in one dimensional array: ");
        value = input.nextInt();
        System.out.print("Please, enter a number for a binary searching in ArrayList: ");
        listValue = input.nextInt();

        index = binarySearch(value, array);
        listIndex = binarySearch(listValue, list);

        System.out.println("\n" + ((index == -1) ? "No result" : "Value " + value + " contains in one dimensional array at index: " + index));
        System.out.println(((index == -1) ? "No result" : "Value " + listValue + " contains in ArrayList at index: " + listIndex));
    }

    /**
     * A method binary searches an {@code middleIndex} of {@code value} in {@code array}.
     *
     * @param value a value for searching.
     * @param array the one dimensional array.
     * @return either an {@code middleIndex} or -1.
     */
    private static int binarySearch(int value, int[] array) {
        int firstIndex = 0;
        int lastIndex = array.length - 1;
        int middleIndex, middleValue;

        while(firstIndex <= lastIndex) {
            middleIndex = (firstIndex + lastIndex) / 2;
            middleValue = array[middleIndex];

            if(middleValue == value) {
                return middleIndex;
            }
            else if(middleValue < value) {
                firstIndex = middleIndex + 1;
            }
            else {
                lastIndex = middleIndex - 1;
            }
        }

        return -1;
    }

    /**
     * A method binary searches an {@code middleIndex} of {@code value} in {@code list}.
     *
     * @param value a value for searching.
     * @param list the ArrayList.
     * @return either an {@code middleIndex} or -1.
     */
    private static int binarySearch(int value, ArrayList<Integer> list) {
        int firstIndex = 0;
        int lastIndex = list.size() - 1;
        int middleIndex, middleValue;

        while(firstIndex <= lastIndex) {
            middleIndex = (firstIndex + lastIndex) / 2;
            middleValue = list.get(middleIndex);

            if(middleValue == value) {
                return middleIndex;
            }
            else if(middleValue < value) {
                firstIndex = middleIndex + 1;
            }
            else {
                lastIndex = middleIndex - 1;
            }
        }

        return 0;
    }
}